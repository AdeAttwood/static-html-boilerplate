// import { partials } from 'handlebars/handlebars.runtime';

const path    = require('path');
const webpack = require('webpack');

const srcPath = path.resolve(__dirname, '.')
const webRoot = srcPath + '/public_html';
const webPath = srcPath + '/public_html/assets';
const assetsPath = srcPath + '/assets';

const loadPaths = [
    assetsPath + '/scss',
    srcPath + '/node_modules'
];

webpackConfig = {
    target: 'web',
    context: assetsPath,
    entry: {
        site: './js/index.js',
    },
    output: {
        path: webPath,
        filename: 'js/[name].js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [
                    /node_modules\/bootbox\/bootbox\.js/
                ],
                use: [
                    {loader: 'amd-define-factory-patcher-loader'},
                ]
            },
            {
                test: /\.js$/,
                use: [
                    {loader: 'babel-loader', options: {presets: ['es2015']}},
                    {loader: 'rollup-loader'}
                ]
            },
            { test: /\.hbs$/, loader: "handlebars-loader" }
        ]
    },
    plugins: [
        // new webpack.optimize.CommonsChunkPlugin('site'),
    ],
    resolve: {
        alias: {
        },
    }
};
    
if (process.env.NODE_ENV === 'production') {
    webpackConfig.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            ie8: false,
            sourcemap: false,
            compress: {
                warnings: false
            },
            output: {
                comments: false
            },
        })
    );

    webpackConfig.plugins.push(
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        })
    );
}

module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
    
    grunt.initConfig({
        vars:{
            changedFile: ""
        },

        sass: {
            dev: {
                update:true,
                options: {
                    includePaths: loadPaths,
                    indentWidth: 4,
                    outputStyle: 'expanded'
                },
                files: [{
                    expand: true,
                    flatten: true,
                    cwd: assetsPath + '/scss',
                    src: ['**/*.scss', '**/*.sass', '**/*.css'],
                    dest: webPath + '/css',
                    ext: '.css'
                }]
            }
        },

        webpack: {
            options: {
                stats: !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
            },
            prod: webpackConfig,
            dev: Object.assign({ watch: false }, webpackConfig)
        },

        copy: {
            fonts: {
                files: [{expand: true, cwd: srcPath + '/assets/fonts', src: '**', dest: webPath + '/fonts', filter: 'isFile'}]
            },
            fontAwesome: {
                files: [{expand: true, cwd: srcPath + '/node_modules/font-awesome/fonts', src: '**', dest: webPath + '/fonts', filter: 'isFile'}]
            }
        },

        watch: {
            js: {
                files: assetsPath + '/js/**/*',
                tasks: ['webpack:dev'],
            },
            sass: {
                files: [ assetsPath + '/scss/**/*' ],
                tasks: ['sass:dev', 'postcss:dev'],
            },
            views: {
                files: [srcPath + '/views/**/*'],
                tasks: ['hbs']
            }
        },

        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        srcPath + 'public_html/**/*',
                    ]
                },
                options: {
                    server: './public_html',
                    open: true,
                    watchTask: true
                }
            }
        },

        postcss: {
            prod: {
                options: {
                    processors: [
                        require('postcss-import')({path:loadPaths}),
                        require('autoprefixer')( { browsers: 'last 2 versions' } ),
                        require("css-mqpacker")(),
                        require('cssnano')( { discardComments:{ removeAll: true } } )
                    ]
                },
                src: webPath + '/css/**/*.css'
            },
            dev: {
                options: {
                    map: false,
                    processors: [
                        require('postcss-import')({path:loadPaths}),
                        require('autoprefixer')({browsers: 'last 2 versions'}),
                        require("css-mqpacker")()
                    ]
                },
                src: webPath + '/css/**/*.css'
            }
        },

        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    cwd: './public_html',
                    src: '**/*.html',
                    dest: './public_html'
                }]
            },
        }

    });

    grunt.registerTask('hbs', () => {
        var handlebars = require('handlebars'),
            layouts = require('handlebars-layouts')
            fs = require('fs')
            glob = require('glob');

        handlebars.registerHelper(layouts(handlebars));

        var layouts = glob('views/layouts/*.hbs', {sync: true});
        var partial = glob('views/includes/*.hbs', {sync: true});
        var views = glob('views/*.hbs', {sync: true});

        layouts.forEach((element) => {
            var filename = element.replace(/^.*[\\\/]/, '').replace('.hbs', '');
            handlebars.registerPartial(filename, fs.readFileSync(element, 'utf8'));
        });

        partial.forEach((element) => {
            var filename = element.replace(/^.*[\\\/]/, '').replace('.hbs', '');
            handlebars.registerPartial(filename, fs.readFileSync(element, 'utf8'));
        });

        views.forEach((element) => {            
            var filename = element.replace(/^.*[\\\/]/, '').replace('.hbs', '');
            var template = handlebars.compile(fs.readFileSync(element, 'utf8'));
            fs.writeFileSync(srcPath+'/public_html/'+filename+'.html', template());
        });
    });

    // change config var to changed file when a file is changed
    grunt.event.on('watch', (action, filepath) => {
        grunt.config('vars.changedFile', filepath);
    });

    grunt.registerTask(
        'buildProd',
        [
            'sass:dev',
            'postcss:prod',
            'webpack:prod',
            'copy:fontAwesome',
            'copy:fonts',
            'hbs',
            'htmlmin'
        ]
    );

    grunt.registerTask(
        'buildDev',
        [
            'sass:dev', 
            'postcss:dev',
            'webpack:dev',
            'copy:fontAwesome',
            'copy:fonts',
            'hbs'
        ]
    );

    grunt.registerTask('build', ['buildProd']);
    grunt.registerTask('default', ['buildDev', 'browserSync:dev', 'watch']);
};