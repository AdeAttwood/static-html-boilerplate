# Static Html Boilerplate

Boilerplate app for rendering a static html site the view use handlebars templates with sass and webpack for assets.

## Installing

~~~ bash
npm install
~~~

## Building

Running browser sync and running the node server for development

~~~ bash
npm start
~~~

Building for production minifying all of the assets and html ready for uploading the `public_html` folder

~~~ bash
npm run production
~~~